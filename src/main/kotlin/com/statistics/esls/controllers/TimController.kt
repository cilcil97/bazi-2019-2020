package com.statistics.esls.controllers

import com.statistics.esls.controllers.requests.IgrachRequest
import com.statistics.esls.service.IgrachService
import com.statistics.esls.service.TimService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/timovi")
@CrossOrigin("*")
class TimController(val service: TimService) {


    @GetMapping()
    fun findAllTimovi() = service.findAll()

    @GetMapping("/{id}")
    fun findTimById(@PathVariable("id") id : Long ) = service.findById(id);


//    @PostMapping("/create")
//    fun createIgrach( @RequestBody request : IgrachRequest) = service.create(request.ime,request.akronim,request.drzava)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable("id") id : Long) = service.deleteById(id)
}