package com.statistics.esls.controllers

import com.statistics.esls.controllers.requests.LigaRequest
import com.statistics.esls.controllers.requests.OrganizacijaRequest
import com.statistics.esls.models.Organizacija
import com.statistics.esls.service.OrganizacijaService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/organizacija")
@CrossOrigin("*")
class OrganizacijaController(val service : OrganizacijaService){


    @GetMapping()
    fun findAllLiga() = service.findAll()

    @GetMapping("/{id}")
    fun findLigaById(@PathVariable("id") id : Long ) = service.findById(id);


    @PostMapping("/create")
    fun createLiga(@RequestBody request : OrganizacijaRequest) = service.createOrganizacija(request.ime)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable("id") id : Long) = service.deleteById(id)

}