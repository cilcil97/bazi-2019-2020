package com.statistics.esls.controllers.requests

import javax.validation.constraints.NotEmpty

class OrganizacijaRequest(

        @NotEmpty
        val ime: String

) {
}