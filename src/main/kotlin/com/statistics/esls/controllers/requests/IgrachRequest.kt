package com.statistics.esls.controllers.requests

import javax.persistence.Column
import javax.validation.constraints.NotEmpty

 class IgrachRequest(

        @NotEmpty
        val ime: String,

        @NotEmpty
        val akronim: String,

        @NotEmpty
        val drzava: String

) {
}