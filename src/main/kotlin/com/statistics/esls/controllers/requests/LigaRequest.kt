package com.statistics.esls.controllers.requests

import javax.validation.constraints.NotEmpty

class LigaRequest(

        @NotEmpty
        val ime: String,

        @NotEmpty
        val kontinent: String,

        @NotEmpty
        val mestoNaOdrzuvanje: String

) {
}