package com.statistics.esls.controllers

import com.statistics.esls.controllers.requests.IgrachRequest
import com.statistics.esls.service.IgrachService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/igrachi")
@CrossOrigin("*")
class IgrachController(val igrachService: IgrachService) {


    @GetMapping()
    fun findAllIgrachi() = igrachService.findAll()

    @GetMapping("/{id}")
    fun findIgrachById( @PathVariable("id") id : Long ) = igrachService.findById(id);


    @PostMapping("/create")
    fun createIgrach( @RequestBody request : IgrachRequest) = igrachService.create(request.ime,request.akronim,request.drzava)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable("id") id : Long) = igrachService.deleteById(id)
}