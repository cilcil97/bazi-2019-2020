package com.statistics.esls.controllers

import com.statistics.esls.controllers.requests.LigaRequest
import com.statistics.esls.service.LigaService
import com.statistics.esls.service.OrganizacijaService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/liga")
@CrossOrigin("*")
class LigaController(val ligaService: LigaService) {


    @GetMapping()
    fun findAllLiga() = ligaService.findAll()

    @GetMapping("/{id}")
    fun findLigaById(@PathVariable("id") id : Long ) = ligaService.findById(id);


    @PostMapping("/create")
    fun createLiga(@RequestBody request : LigaRequest) = ligaService.create(request.ime,request.kontinent,request.mestoNaOdrzuvanje)

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable("id") id : Long) = ligaService.deleteById(id)
}