package com.statistics.esls.service

import com.statistics.esls.models.Igrach
import com.statistics.esls.models.Liga
import com.statistics.esls.repository.LigaRepository
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class LigaService(val repository:LigaRepository) {


    fun findAll() = repository.findAll()

    fun findById(id:Long ) = repository.findById(id).orElseThrow{ RuntimeException("Nema takva Liga so id $id") }

    fun deleteById(id :Long) {
        val liga = this.findById(id)
        repository.delete(liga)
    }

    fun create(ime:String,kontinent:String,mestoOdrzuvanje:String): Liga {
        val liga = Liga(0,ime,kontinent,mestoOdrzuvanje, emptyList())
        return repository.save(liga)
    }

}