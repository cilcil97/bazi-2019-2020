package com.statistics.esls.service

import com.statistics.esls.models.Igrach
import com.statistics.esls.repository.TimRepository
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class TimService (val repository: TimRepository) {

    fun findAll() = repository.findAll()

    fun findById(id:Long ) = repository.findById(id).orElseThrow{ RuntimeException("Nema takov tim so id $id") }

    fun deleteById(id :Long) {
        val tim = this.findById(id)
        repository.delete(tim)
    }

//    fun create(ime:String,akronim:String,drzava:String): Igrach {
//        val tim = tim(0,ime,akronim,drzava)
//        return repository.save(tim)
//    }

}