package com.statistics.esls.service

import com.statistics.esls.models.Liga
import com.statistics.esls.models.Organizacija
import com.statistics.esls.repository.LigaRepository
import com.statistics.esls.repository.OrganizacijaRepository
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class OrganizacijaService(val repository: OrganizacijaRepository) {


    fun findAll() = repository.findAll()

    fun findById(id:Long ) = repository.findById(id).orElseThrow{ RuntimeException("Nema takva Liga so id $id") }

    fun deleteById(id :Long) {
        val liga = this.findById(id)
        repository.delete(liga)
    }

    fun createOrganizacija(ime:String): Organizacija {
        val organizacija = Organizacija(0,ime, emptyList())
        return repository.save(organizacija)
    }

}