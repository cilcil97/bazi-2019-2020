package com.statistics.esls.service

import com.statistics.esls.models.Igrach
import com.statistics.esls.repository.IgrachRepository
import org.springframework.stereotype.Service
import java.lang.RuntimeException

@Service
class IgrachService ( val repository:IgrachRepository) {

    fun findAll() = repository.findAll()

    fun findById(id:Long ) = repository.findById(id).orElseThrow{RuntimeException("Nema takov igrach so id $id")}

    fun deleteById(id :Long) {
        val igrach = this.findById(id)
        repository.delete(igrach)
    }

    fun create(ime:String,akronim:String,drzava:String): Igrach {
        val igrach = Igrach(0,ime,akronim,drzava)
        return repository.save(igrach)
    }


}