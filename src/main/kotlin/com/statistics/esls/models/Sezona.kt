package com.statistics.esls.models

import java.time.ZonedDateTime
import java.util.*
import javax.persistence.*

@Entity()
@Table(schema = "projects",name = "sezona")
data class Sezona (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Long = 0,

        @Column(unique = true)
        val year:Date,

        @Column(unique = true)
        val startDate:Date,

        @Column(unique = true)
        val endDate:Date

        ){
}