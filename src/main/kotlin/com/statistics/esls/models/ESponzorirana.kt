package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema = "projects",name = "e_sponzorirana")
data class ESponzorirana(
        @Id
        @GeneratedValue( strategy = GenerationType.IDENTITY)
        val id : Long = 0,

        @Column(name = "pari")
        val pari : Int,

        @ManyToOne
        val organizacija: Organizacija,

        @ManyToOne
        val investor: Investor



) {
}