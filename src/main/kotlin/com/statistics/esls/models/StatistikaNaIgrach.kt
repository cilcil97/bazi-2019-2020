package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema = "projects",name = "statistika_na_igrach")
data class StatistikaNaIgrach (

        @Id
        @GeneratedValue
        val id : Long = 0,

        @Column(name = "umreni")
        val umreni : Int,

        @Column(name = "ubistva")
        val ubistva : Int,

        @Column(name = "asistencii")
        val asistencii:Int,

        @Column(name = "poeni")
        val poeni : Int,

        @ManyToOne
        val utakmica: Utakmica,

        @ManyToOne
        val Igrach : Igrach



){
}