package com.statistics.esls.models

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*


@Entity
@Table(schema = "projects",name = "organizacii")
data class Organizacija(

        @Id
        @GeneratedValue( strategy = GenerationType.IDENTITY)
        @Column("oid")
        val id : Long = 0,

        @Column(name = "ime")
        val ime : String,

        @OneToMany(mappedBy = "organizacija")
        @JsonIgnore
        val timovi : List<Tim>

) {
}