package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table( schema = "projects", name = "igrachi")
data class Igrach(
        @Id()
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "iid")
        val id: Long = 0,

        @Column(name = "ime")
        val ime: String,

        @Column(name = "akronim")
        val akronim: String,

        @Column(name = "drzava")
        val drzava: String

)