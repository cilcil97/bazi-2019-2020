package com.statistics.esls.models

import java.util.*
import javax.persistence.*

@Entity
@Table(schema = "projects", name = "menuva_pozicija" )
data class MenuvaPozicija(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id :Long = 0,

        @ManyToOne
        val igrach: Igrach,

        @ManyToOne
        val pozicija: Pozicija,

        @Column(name = "datum_smena")
        val datumSmena : Date



)
{
}