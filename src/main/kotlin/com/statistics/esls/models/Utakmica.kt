package com.statistics.esls.models

import javax.persistence.*


@Entity
@Table(schema = "projects",name = "utakmica")
data class Utakmica(

        @Id
        @GeneratedValue
        @Column(name = "uid")
        val id : Long = 0,

        @ManyToOne
        val tim1: Tim,

        @ManyToOne
        val tim2: Tim,

        @ManyToOne
        val ligaSezona: LigaSezona,

        @Column(name = "broj_gledachi")
        val brojGledachi : Int

) {
}