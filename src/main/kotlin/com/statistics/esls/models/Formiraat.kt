package com.statistics.esls.models

import java.time.Year
import javax.persistence.*

@Entity
@Table(schema = "projects",name = "formiraat")
data class Formiraat(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id : Long = 0,

    @ManyToOne
    val year: Sezona,

    @ManyToOne
    val tim: Tim,

    @ManyToOne
    val igrach: Igrach

) {
}