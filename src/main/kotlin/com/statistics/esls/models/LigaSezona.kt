package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema = "projects",name = "liga_sezona")
data class LigaSezona(

        @Id
        @GeneratedValue(strategy =GenerationType.IDENTITY)
        @Column(name = "lsid")
        val id : Long = 0 ,


        @ManyToOne
        val liga : Liga,

        @ManyToOne
        val sezona: Sezona


){}