package com.statistics.esls.models

import java.util.*
import javax.persistence.*

@Entity
@Table(schema = "projects",name = "menuvaa_tim")
data class MenuvaaTim(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id:Long = 0 ,

        @ManyToOne
        val tim: Tim,

        @ManyToOne
        val igrach: Igrach,

        @Column(name = "cena")
        val cena : Int,

        @Column(name = "datum_na_smena")
        val datumNaSmena : Date
) {
}