package com.statistics.esls.models

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(schema = "projects", name = "liga")
 data class Liga(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "lid")
        val id: Long = 0,

        @Column(name = "kontinent")
        val kontinent: String,

        @Column(name = "ime")
        val ime: String,

        @Column(name = "mesto_na_odrzuvanje")
        val mestoNaOdrzuvanje: String,

        @OneToMany(mappedBy = "liga")
        @JsonIgnore
        val timovi: List<Tim>


)