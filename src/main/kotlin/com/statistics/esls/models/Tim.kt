package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema="projects",name = "tim")
data class Tim(
        @Id
        @GeneratedValue
        @Column(name = "tid")
        val id :Long = 0,

        @Column(name = "ime")
        val ime : String,

        @Column(name = "logo")
        val logo : String,

        @ManyToOne()
        val liga:Liga,

        @ManyToOne()
        val organizacija:Organizacija


) {
}