package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema = "projects", name = "projects")
data class Pozicija (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "pid")
        val id : Long = 0,

        @Column("ime")
        val ime:String

){
}