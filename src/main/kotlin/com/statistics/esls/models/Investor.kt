package com.statistics.esls.models

import javax.persistence.*

@Entity
@Table(schema  = "projects",name = "investori")
data class Investor (

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id : Long = 0,

        @Column(name = "ime")
        val ime : String,

        @Column(name = "tip")
        val tip : String

) {
}