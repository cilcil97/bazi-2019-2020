package com.statistics.esls.repository

import com.statistics.esls.models.ESponzorirana
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ESponzorirana : JpaRepository<ESponzorirana,Long> {
}