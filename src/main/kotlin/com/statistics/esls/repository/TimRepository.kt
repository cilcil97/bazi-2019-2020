package com.statistics.esls.repository

import com.statistics.esls.models.Tim
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TimRepository : JpaRepository<Tim,Long> {
}