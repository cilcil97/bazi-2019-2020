package com.statistics.esls.repository

import com.statistics.esls.models.MenuvaaTim
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MenuvaaTimRepository : JpaRepository<MenuvaaTim,Long> {
}