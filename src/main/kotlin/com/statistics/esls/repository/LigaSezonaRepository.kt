package com.statistics.esls.repository

import com.statistics.esls.models.LigaSezona
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LigaSezonaRepository : JpaRepository<LigaSezona,Long> {
}