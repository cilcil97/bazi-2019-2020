package com.statistics.esls.repository

import com.statistics.esls.models.MenuvaPozicija
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


@Repository
interface MenuvaPozicijaRepository  : JpaRepository<MenuvaPozicija,Long>{
}