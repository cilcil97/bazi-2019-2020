package com.statistics.esls.repository

import com.statistics.esls.models.Formiraat
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FormiraatRepository : JpaRepository<Formiraat,Long> {
}