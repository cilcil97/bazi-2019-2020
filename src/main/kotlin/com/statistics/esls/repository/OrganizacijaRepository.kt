package com.statistics.esls.repository

import com.statistics.esls.models.Organizacija
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrganizacijaRepository : JpaRepository<Organizacija,Long> {
}