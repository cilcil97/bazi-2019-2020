package com.statistics.esls.repository

import com.statistics.esls.models.Sezona
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SezonaRepository : JpaRepository<Sezona,Long> {
}