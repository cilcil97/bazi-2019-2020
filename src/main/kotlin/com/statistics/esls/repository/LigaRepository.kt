package com.statistics.esls.repository

import com.statistics.esls.models.Liga
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LigaRepository : JpaRepository<Liga,Long>{
}