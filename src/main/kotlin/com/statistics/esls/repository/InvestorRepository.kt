package com.statistics.esls.repository

import com.statistics.esls.models.Investor
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InvestorRepository : JpaRepository<Investor,Long> {
}