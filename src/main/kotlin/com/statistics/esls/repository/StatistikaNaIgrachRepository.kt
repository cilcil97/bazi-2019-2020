package com.statistics.esls.repository

import com.statistics.esls.models.StatistikaNaIgrach
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface StatistikaNaIgrachRepository  :JpaRepository<StatistikaNaIgrach,Long>{
}