package com.statistics.esls.repository

import com.statistics.esls.models.Igrach
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IgrachRepository : JpaRepository<Igrach,Long> {
}