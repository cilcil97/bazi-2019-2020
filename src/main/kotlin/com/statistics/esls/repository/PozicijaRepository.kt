package com.statistics.esls.repository

import com.statistics.esls.models.Pozicija
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface PozicijaRepository  : JpaRepository<Pozicija,Long>{
}