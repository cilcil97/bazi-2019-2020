package com.statistics.esls

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EslsApplication

fun main(args: Array<String>) {
    runApplication<EslsApplication>(*args)
}
